package fulfillsaddle.stub;
/* */
public interface FulfillSaddleFactory {
	FulfillSaddleStub createFulfillSaddle() throws Exception;
}
