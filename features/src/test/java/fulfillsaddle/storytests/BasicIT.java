package fulfillsaddle.storytests;

import fulfillsaddle.stub.*;

import cucumber.api.java.Before;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.*;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.net.URL;

public class BasicIT {

	private String instance_saddleId = null;

	private String fulfillSaddleFactoryClassName = System.getenv("FULFILL_SADDLE_FACTORY_CLASS_NAME");
	private FulfillSaddleFactory fsFac;
	private FulfillSaddleStub fs;

	private String jdbcPort = System.getenv("FULFILL_SADDLE_MYSQL_HOST_PORT");
	private String jdbcUrl = "jdbc:mysql://localhost:" + this.jdbcPort + "/FulfillSaddle?connectTimeout=0&socketTimeout=0&autoReconnect=true";
	private String jdbcUser = System.getenv("FULFILL_SADDLE_MYSQL_USER");
	private String jdbcPass = System.getenv("FULFILL_SADDLE_MYSQL_PASSWORD");

	public BasicIT() {
		if (fulfillSaddleFactoryClassName == null) throw new Error(
			"Env variable FULFILL_SADDLE_FACTORY_CLASS_NAME not set");
		try {
			fsFac = (FulfillSaddleFactory)(Class.forName(fulfillSaddleFactoryClassName).getDeclaredConstructor().newInstance());
			fs = fsFac.createFulfillSaddle();
		} catch (Exception ex) {
			throw new RuntimeException("During startup", ex);
		}
		System.err.println("Constructed FulfillSaddle stub");

		if (this.jdbcPort == null) throw new RuntimeException("Env variable FULFILL_SADDLE_MYSQL_HOST_PORT not set");
		if (this.jdbcUser == null) throw new RuntimeException("Env variable FULFILL_SADDLE_MYSQL_USER not set");
		if (this.jdbcPass == null) throw new RuntimeException("Env variable FULFILL_SADDLE_MYSQL_PASSWORD not set");
	}


	/* Scenario Add To Inventory */

	@Given("that the database does not contain {string}")
	public void database_does_not_contain(String saddleId) throws Exception {
		purgeDb();
	}

	@When("I call AddToInventory for {string}")
	public void add_to_inventory(String saddleId) throws Exception {
		System.err.println("Entered add_to_inventory...");

		System.err.println("1 About to call addToInventory(" + saddleId + ")");
		fs.addToInventory(saddleId);
		System.err.println("...returned from calling addToInventory");

		this.instance_saddleId = saddleId; // save for next step
	}

	@Then("a subsequent call to AddToInventory for {string} will return an error")
	public void does_not_return_an_error(String saddleId) throws Exception {

        try {
			System.err.println("2 About to call addToInventory(" + saddleId + ")");
			fs.addToInventory(saddleId);
			System.err.println("...returned from calling addToInventory");
		} catch (Exception ex) {
			System.err.println("Add to Inventory scenario passed");
			return;
		}
		throw new Exception("AddToInventory succeeded but should not have - saddle should be in inventory already");
	}

	/* Scenario Requisition */

	@Given("that the database contains only {string}")
	public void populate(String saddleId) throws Exception {

		System.err.println("Entered populate...");
		purgeDb();

		System.err.println("3 About to call addToInventory(" + saddleId + ")");
		fs.addToInventory(saddleId);
		System.err.println("...returned from calling addToInventory");
	}

	@When("I call RequisitionFromInventory with department number {int}")
	public void call_place_order(int deptNo) throws Exception {
		System.err.println("Entered call_place_order...");

		String saddleId = null;
		try {
			System.err.println("About to call requisitionFromInventory(" + deptNo + ")");
			saddleId = fs.requisitionFromInventory(deptNo);
			System.err.println("...returned from calling requisitionFromInventory");
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
			throw ex;
		}

		this.instance_saddleId = saddleId;
	}

	@Then("it returns SaddleID {string}")
	public void returns_a_saddle_id(String saddleId) throws Exception {
		if (! (saddleId.equals(this.instance_saddleId))) throw new Exception(
			"Saddle Id returned is " + saddleId + " instead of " + this.instance_saddleId);
		System.err.println("Requisition scenario passed");
	}

	private void purgeDb() throws Exception {
		try {
			System.err.println("Getting connection to database...");
			Connection conn = DriverManager.getConnection(db, jdbcUser, jdbcPass);
			if (conn == null) throw new Exception("Unable to connect to FulfillSaddle database");
			Statement stmt = conn.createStatement();
			System.err.println("About to truncate FulfillSaddle...");
			stmt.executeUpdate("TRUNCATE TABLE FulfillSaddle");
			System.err.println("...truncated FulfillSaddle.");
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
			throw ex;
		}
	}
}
